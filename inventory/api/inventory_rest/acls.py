from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(manufacturer, car):
  pexels_url = "https://api.pexels.com/v1/search"
  params = {"per_page": 1, "query": manufacturer + " " + car}

  pexels_headers = {"Authorization": PEXELS_API_KEY}
  pexels_r = requests.get(pexels_url, params=params, headers=pexels_headers)
  pexels_content = json.loads(pexels_r.content)
  try: 
    return {"picture_url": pexels_content["photos"][0]["src"]["original"]}
  except (KeyError, IndexError):
    return {"picture_url": None}
# CarCar

### Team:

* James Leung - Sales microservice
* Jesse Preble - Service microservice

### Design:
```
CarCar
|
| - React (ghi\app)
|   | - Components
|
| - Inventory
|   | - Manufacturers
|   | - Models
|   | - Automobiles
|
| - Sales (BC)
|   | - Salespeople
|   | - Customers
|   | - Sales
|   |   | - AutomobileVOs (VO)
|   | - Sale Poller -> Inventory (for AutomobileVOs)
|
| - Service
|   | - Technicians
|   | - Appointments
|   |   | - CustomerVOs (VO)
|   |   | - AutomobileVOs (VO)
|   | - Service Poller -> Inventory (for AutomobileVOs)
|   | - Service Poller -> Sales (for CustomerVOs)

____ ___________________________
____|CarCar App                 |
____|___________________________|
____|Inventory Bounded Context  |
____|Manufacturers              |
____|Models                     |
____|Automobiles                |
____|      |VO--------------|   |
____|______V________________V___|_______________
|Sales Bounded Context |Services Bounded Context|
|Salespeople           |Technicians             |
|Customers---------VO--> VO: Customers          |
|Sales                 |Appointments            |
|VO: Automobile        |VO: Automobiles         |
|Poller                |Poller                  |
|______________________|________________________|
```
#### To start:

- Fork the repository at https://gitlab.com/Karisuta/carcar
- Open terminal (if not already opened).
- Clone the repository to local drive with the following command
 - git clone https://gitlab.com/"your-username-here"/carcar
- Change directory into the newly created repository
  - cd carcar
- With docker desktop running, run the following commands in terminal to build docker volume/containers and start the containers:
  - docker volume create beta-data
  - docker-compose build
  - docker-compose up
- You may need to wait a few minutes for the react container to finish booting up. To check you can click into the container named react-1 in docker desktop and view the logs. You'll know it's ready when the latest message is "webpack compiled with 1 warning"
- Open http://localhost:3000/ in browser.

#### To Test Functionality:

Look at top nav bar. There are three drop down menus: Inventory, Sales, and Service.
 - Clicking on the Inventory dropdown will provide links to: Manufacturers, New Manufacturer, Models, New Model, Automobiles, New Automobile.
 - Clicking on the Sales dropdown will provide links to: Salespeople, New Salesperson, Customers, New Customer, Sales, New Sale, Sales History.
 - Clicking on the Service dropdown will provide links to: Technicians, New Technician, Appointments, New Appointment, Service History

Click 'New Manufacturer', fill out appropriate fields using characters, click 'Add manufacturer'. Check that new manufacturer was made by clicking 'Manufacturers'.
Click 'New Vehicle Model', fill out appropriate fields using characters and select manufacturer, click 'Add model'. Check that new model was made by clicking 'Vehicle Models'.

Click 'New Automobile', fill out the fields with the specified information, then click 'Add' to create the automobile. If an automobile was successfully created, the user should be rerouted back to the list of automobiles ('Automobiles' in the nav bar). There, check to make sure the automobile has been added to the list.

Click 'New Salesperson', fill out appropriate fields using characters, click 'Add salesman'. Check that new salesman was made by clicking 'Salespeople'.

Click 'New Customer', fill out appropriate fields using characters, click 'Add customer'. Check that new customer was made by clicking 'Customers'.

Click 'New Sale', select an unsold automobile (list already filters to show only unsold automobiles), salesperson, and customer, fill in price with integers, click 'Add sale'. Check that new sale was made by clicking 'Sales'. This will also update the automobile to be sold. You can also delete a sale via the sales list page, and it will update the automobile to be not sold.

Click 'Sales History' to check record of sales, with the capability of filtering by salesman.

Click 'New Technician', fill out the fields with the specified information, then click 'Create' to create a new technician.

 If a technician was successfully created, the user should be rerouted back to the list of technicians ('Technicians' in the nav bar). There, check to make sure the technician has been added to the list.

Click 'New Appointments', fill out the fields with the specified information and select a technician from the drop down menu. Then click 'Add' to add a service appointment. If an appointment was successfully added, the page should reroute to the list of appointments ('Appointments' in the nav bar). There, check to make sure the appointment was successfully added to the list.


 |||||

### Inventory: Port 8100
The inventory microservice contains the bounded contexts of manufacturers, vehicle models, and automobiles. There is information about the manufacturers, vehicle models, and automobiles displayed as a list and forms to create new entries for each. Each list and form view is formed from a React component in the src folder of the React app, where data is pulled from the Inventory database via fetch API and then transmitted to the page.

Manufacturers:
  - List url: http://localhost:3000/api/manufacturers/
  - Form url: http://localhost:3000/api/manufacturers/new

Models:

  - List url: http://localhost:3000/api/models/
  - Form url: http://localhost:3000/api/models/new

Automobiles:
  - List url: http://localhost:3000/api/automobiles/
  - Form url: http://localhost:3000/api/automobiles/new/

|||||

#### CRUD routes: Port 8100
  Manufacturers:

  To test functionality in Insomnia, run code in following order:

  Create a manufacturer (POST):
  http://localhost:8100/api/manufacturers/

  Example JSON body:
```
  {
    "name": "Toyota"
  }
```
  List manufacturers (GET):
  http://localhost:8100/api/manufacturers/

  Example Response:
```
{
    "manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Toyota"
		}
	]
}
```

  Get a specific manufacturer (GET):
  http://localhost:8100/api/manufacturers/:id/

  Example url:
  http://localhost:8100/api/manufacturers/1/

  Example Response:
  ```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}
  ```

  Update a specific manufacturer (PUT):
  http://localhost:8100/api/manufacturers/:id/

  Example url:
  http://localhost:8100/api/manufacturers/1/

  Example JSON body:
  ```
  {
    "name": "Toyota"
  }
```
  Delete a specific manufacturer (DELETE):
  http://localhost:8100/api/models/:id/

  Example url:
  http://localhost:8100/api/models/1/

  List manufacturers again to check (GET):
  http://localhost:8100/api/manufacturers/

|||||

  Models:

  To test functionality in Insomnia, run code in following order:

  Create a model (POST):
  http://localhost:8100/api/models/

  Example JSON body:
```
  {
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer_id": 1
  }
```
  List models (GET):
  http://localhost:8100/api/models/

  Example Response:
  ```
  {
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Camry",
			"picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpY6SRfDk02eFWKrF8R2rSbBcn6NgvfTpX9m-au5OcRS9BNjsN",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Toyota"
			}
		}
	]
}
  ```

  Get a specific model (GET):
  http://localhost:8100/api/models/:id/

  Example url:
  http://localhost:8100/api/models/1/

  Example Response:
  ```
  {
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```

  Update a specific model (PUT):
  http://localhost:8100/api/models/:id/

  Example url:
  http://localhost:8100/api/models/1/

  Example JSON body:
```
  {
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer_id": 1
  }
```
Expected response same as above

  Delete a specific model (DELETE):
  http://localhost:8100/api/models/:id/

  Example url:
  http://localhost:8100/api/models/1/

  List models again to check (GET):
  http://localhost:8100/api/models/

|||||

  Automobiles:

  To test the functionality of automobiles in Insomnia:

  Create Automobile (POST):
  http://localhost:8100/api/automobiles/

  Example JSON body:
```
    {
      "color": "red",
      "year": 2012,
      "vin": "1C3CC5FB2AN100000",
      "model_id": 1
    }
```
  List Automobiles (GET):
  http://localhost:8100/api/automobiles/

  Expected Response:
```
  {
	"autos": [
		{
			"href": "/api/automobiles/1CN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1CN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Camry",
				"picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpY6SRfDk02eFWKrF8R2rSbBcn6NgvfTpX9m-au5OcRS9BNjsN",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Toyota"
				}
			},
			"sold": true
		}
	]
}
```

  Show an Automobile's Details (GET):
  http://localhost:8100/api/automobiles/vin/

  Replace vin with the vin of the desired automobile

Expected Response:
```
{
	"href": "/api/automobiles/1CN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1CN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Camry",
		"picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpY6SRfDk02eFWKrF8R2rSbBcn6NgvfTpX9m-au5OcRS9BNjsN",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Toyota"
		}
	},
	"sold": true
}
```

  Update Automobile (PUT):
  http://localhost:8100/api/automobiles/vin/

  Replace vin with the vin of the desired automobile

  Example JSON body:
```
    {
      "color": "blue",
      "year": 2012,
      "vin": "1C3CC5FB2AN100000",
      "model_id": 1
    }
```
Expected response same as above

  Delete Automobile
  http://localhost:8100/api/automobiles/id/

  Replace vin with the vin of the desired automobile

### Service microservice:

#### Models:

The Service microservice is composed of a Technician model, an Appointment model, a CustomerVO model which is a value object, and an AutomobileVO model which is also a value object.

The technician model is composed of three fields: first_name, last_name, and employee_id.

The appointment model is composed of date_time, reason, status (which is 'created' on default and will be updated to canceled or finished by a button on the 'Appointments' page), vin, vip, customer which has CustomerVO as a foreignkey, and technician which has Technician as a foreignkey. The appointment model also contains some functions that will be called in views for updating status and for checking if the vin is a vip (if the vin for an appointment matches up to a vehicle in the inventory)

The CustomerVO is composed of first_name and last_name. CustomerVO objects are created by the poller as it polls the sales microservice for customer data. This is used when creating a new appointment to match the customer name inputted to an existing customer (and provide an error if the name does not match an existing customer)

The AutomobileVO is only composed of a vin. AutomobileVOs are created by the poller when it polls the inventory microservice for automobile data. This is used to compare the vin entered in an appointment object to check if the vin matches an automobileVO vin, and if so set the vip field of that appointment to true.

#### View functions:

The APIs for the service microservice are in the views.py file located at service/api/service_rest/views.py. It contains encoders for how the data will be sent from the APIs. It also contains the APIs for getting a list of technicians, creating a technician, deleting a technician, getting a list of appointments, creating an appointment, deleting an appointment, updating an appointments status to canceled, and updating an appointments status to finished. The api_list_appointments function is also the function that checks to see if an appointment is a VIP and if the customer name when creating an appointment matches up to a customerVO name.

#### Poller:

The poller polls the inventory microservice at url 'http://project-beta-inventory-api-1:8000/api/automobiles' to recieve automobile data and update or create automobileVOs
It also polls the sales microservice at url 'http://project-beta-sales-api-1:8000/api/customers' to receive customer data and to then update or create customerVOs.
The poller polls every 60 seconds.

#### Service CRUD routes: Port 8080

  List Technicians (GET):
  http://localhost:8080/api/technicians/

  Expected response example:
  ```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Sam",
			"last_name": "Ple",
			"employee_id": "12345"
		},
		{
			"id": 2,
			"first_name": "Test",
			"last_name": "Person",
			"employee_id": "12346"
		}
	]
}
  ```

  Create Technician (POST):
  http://localhost:8080/api/technicians/

  Example JSON body:
```
    {
      "first_name": "Sam",
      "last_name": "Ple",
      "employee_id": 1234
    }
```
  Expected response same as above

  Delete Technician (DELETE):
  http://localhost:8080/api/technicians/id

  Replace id with the id of the desired technician to be deleted

  Expected response
  ```
  {"deleted": true}
  ```
  if the technician exists and
  ```
  {"deleted": false}
  ```
  if they do not exist or have already been deleted.

  List Appointments (GET):
  http://localhost:8080/api/appointments/

  Expected response example:
  ```
  {
	"appointments": [
		{
			"id": 32,
			"date_time": "2023-05-04T12:30:00+00:00",
			"reason": "Testing",
			"status": "finished",
			"vip": true,
			"vin": "1C3CC5FB2AN120174",
			"technician": {
				"first_name": "Test",
				"last_name": "Person"
			},
			"customer": {
				"first_name": "Sam",
				"last_name": "Ple"
			}
		},
		{
			"id": 33,
			"date_time": "2023-05-04T12:30:00+00:00",
			"reason": "Testing",
			"status": "finished",
			"vip": false,
			"vin": "1C3CC5FB2AN199999",
			"technician": {
				"first_name": "Test",
				"last_name": "Person"
			},
			"customer": {
				"first_name": "Joe",
				"last_name": "Ple"
			}
		}
  ]
  }
  ```

  Create Appointment (POST):
  http://localhost:8080/api/appointments/

  Example JSON body:
```
  {
    "date_time": "2022-09-06T12:30",
    "reason": "10k mile service",
    "vin": "1C3CC5FB2AN120174",
    "customer": "Joe Bob",
    "technician": 1
  }
```

  Example response:
```
  {
    "id": 1,
    "date_time": "2022-09-06T12:30",
    "reason": "10k mile service",
    "status": "created",
    "vip": true
    "vin": "1C3CC5FB2AN120174",
    "technician": {
      "first_name": "Sam",
      "last_name": "Ple"
    },
    "customer": {
        "first_name": "Joe",
        "last_name": "Bob"
    }
  }
```
  vip will be true/false depending on if the vin entered matches an existing automobile VO vin.

  Delete Appointment (DELETE):
http://localhost:8080/api/appointments/id

  Replace id with the id of the desired appointment to be deleted

  Expected response
  ```
  {"deleted": true}
  ```
  if the appointment exists and
  ```
  {"deleted": false}
  ```
  if it does not exist or had already been deleted.

  Appointment Canceled (PUT):
  http://localhost:8080/api/appointments/id/cancel

  Replace id with the id of the desired appointment to change the status of

  Expected response example:
```
  {
    "id": 1,
    "date_time": "2022-09-06T12:30",
    "reason": "10k mile service",
    "status": "canceled",
    "vip": true
    "vin": "1C3CC5FB2AN120174",
    "technician": {
      "first_name": "Sam",
      "last_name": "Ple"
    },
    "customer": {
        "first_name": "Joe",
        "last_name": "Bob"
    }
  }
```
  Appointment Finished (PUT):
  http://localhost:8080/api/appointments/id/finish

  Replace id with the id of the desired appointment to change the status of

  Expected response example:
```
  {
    "id": 1,
    "date_time": "2022-09-06T12:30",
    "reason": "10k mile service",
    "status": "finished",
    "vip": true
    "vin": "1C3CC5FB2AN120174",
    "technician": {
      "first_name": "Sam",
      "last_name": "Ple"
    },
    "customer": {
        "first_name": "Joe",
        "last_name": "Bob"
    }
  }
```
### Sales microservice:

#### Models:
There is a Salesperson, Customer, and Sale model used by the Sales app. There is also a VO for the automobile that is polled for every 60 seconds via the poller in sales/poll/poller.py, then received from the Inventory app on "http://project-beta-inventory-api-1:8000/api/automobiles/".

The AutomobileVO model contains an import_href, and the VIN associated with the automobile. It is used in the Sale model. When creating a new automobile, you may need to wait for the poller to grab the data before using it.

The Salesperon model contains a first_name, last_name, employee_id field. It is used in the Sale model.

The Customer model contains a first_name, last_name, address, and phone_number field. It is used in the Sale model.

The Sale model contains a automobile, salesperson, customer and price field, where automobile, salesperson and customer are all foreign key fields.

#### View functions:

The view functions for the Sales app are located in sales_rest/api_views.py, and the urls are located in sales_rest/api_urls.py.

'api_list_salespeople' handles only the list of salespeople (GET) and making a new salesperson (POST).

'api_show_salespeople' handles only the detail view of a salesperson (GET), updating a salesperson (PUT), or deleting a salesperson (DELETE).

#### Sales CRUD routes: Port 8090

  Salespeople:

  List salespeople (GET):
  http://localhost:8090/api/salespeople/

Expected response:
```
{
	"salespeople": [
		{
			"href": "/api/salespeople/1/",
			"id": 1,
			"first_name": "Jame",
			"last_name": "Leun",
			"employee_id": "jl002"
		},
		{
			"href": "/api/salespeople/4/",
			"id": 4,
			"first_name": "Jamesss",
			"last_name": "Leungss",
			"employee_id": "jl005"
		}
	]
}
```

  Create a salesperson (POST):
  http://localhost:8090/api/salespeople/

  Sample Post data:
```
  {
    "first_name": "James",
    "last_name": "Leung",
    "employee_id": "jl001"
  }
```

Expected response:
```
{
	"href": "/api/salespeople/4/",
	"id": 4,
	"first_name": "Jamesss",
	"last_name": "Leungss",
	"employee_id": "jl005"
}
```

  Delete a specific salesperson	(DELETE):
  http://localhost:8090/api/salespeople/:id

  Example url: http://localhost:8090/api/salespeople/1/

Expected response:
```
{
	"deleted": true,
	"breakdown": {
		"sales_rest.Salesperson": 1
	}
}
```

|||||

  Customers:

  List customers	(GET):
  http://localhost:8090/api/customers/

  Expected response:
```
{
	"customers": [
		{
			"href": "/api/customers/2/",
			"id": 2,
			"first_name": "James",
			"last_name": "Leung",
			"address": "someplace",
			"phone_number": "123456"
		}
	]
}
```

  Create a customer	(POST):
  http://localhost:8090/api/customers/

  Sample Post data:
```
  {
    "first_name": "James",
    "last_name": "Leung",
    "address": "someplaces",
    "phone_number": "1234525345"
  }
```

Expected response:
```
{
	"href": "/api/customers/2/",
	"id": 2,
	"first_name": "James",
	"last_name": "Leung",
	"address": "someplace",
	"phone_number": "123456"
}
```

  Delete a specific customer (DELETE):
  http://localhost:8090/api/customers/:id

  Example url: http://localhost:8090/api/customers/1/

  Expected response:
```
{
	"deleted": true,
	"breakdown": {
		"sales_rest.Customer": 1
	}
}
```

|||||

  Sales:

  List sales (GET):
  http://localhost:8090/api/sales/

  Expected response:
```
{
	"sales": [
		{
			"href": "/api/sales/2/",
			"price": 1123123,
			"automobile": {
				"vin": "1CN120175",
				"import_href": "/api/automobiles/1CN120175/"
			},
			"salesperson": {
				"href": "/api/salespeople/1/",
				"id": 1,
				"first_name": "Jame",
				"last_name": "Leun",
				"employee_id": "jl002"
			},
			"customer": {
				"href": "/api/customers/1/",
				"id": 1,
				"first_name": "Jamess",
				"last_name": "Leungs",
				"address": "someplacesss",
				"phone_number": "12345325435345"
			}
		}
	]
}
```

  Create a sale	(POST):
http://localhost:8090/api/sales/

  Sample Post data:
```
  {
    "automobile": "/api/automobiles/1C3CC5FB2AN120174/",
    "salesperson": 1,
    "customer": 1,
    "price": 8000
  }
```

Expected response:
```
{
	"href": "/api/sales/5/",
	"price": 8000,
	"automobile": {
		"vin": "1CN120175",
		"import_href": "/api/automobiles/1CN120175/"
	},
	"salesperson": {
		"href": "/api/salespeople/1/",
		"id": 1,
		"first_name": "Jame",
		"last_name": "Leun",
		"employee_id": "jl002"
	},
	"customer": {
		"href": "/api/customers/2/",
		"id": 2,
		"first_name": "James",
		"last_name": "Leung",
		"address": "someplace",
		"phone_number": "123456"
	}
}
```

  Delete a sale	(DELETE):
  http://localhost:8090/api/sales/:id

  Example url: http://localhost:8090/api/sales/1/

  Expected response:
```
{
	"deleted": true,
	"breakdown": {
		"sales_rest.Sale": 1
	}
}
```

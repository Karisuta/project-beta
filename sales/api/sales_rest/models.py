from django.db import models
from django.urls import reverse

class Salesperson(models.Model):
  first_name = models.CharField(max_length=200)
  last_name = models.CharField(max_length=200)
  employee_id = models.CharField(max_length=100)

  def __str__(self):
    return f"{self.first_name} {self.last_name}"
  class Meta:
    ordering = ("first_name",)

  def get_api_url(self):
    return reverse("api_show_salespeople", kwargs={"pk": self.pk})
    
class Customer(models.Model):
  first_name = models.CharField(max_length=200)
  last_name = models.CharField(max_length=200)
  address = models.CharField(max_length=100)
  phone_number = models.CharField(max_length=100,null=True)

  def __str__(self):
    return f"{self.first_name} {self.last_name}"
  class Meta:
    ordering = ("first_name",)

  def get_api_url(self):
    return reverse("api_show_customer", kwargs={"pk": self.pk}) 

class AutomobileVO(models.Model):
  import_href = models.CharField(max_length=200, unique=True)
  vin = models.CharField(max_length=200)

  def __str__(self):
    return f"{self.vin}"

class Sale(models.Model):
  sales_id = models.CharField(max_length=200)
  price = models.IntegerField(null=True)

  automobile = models.ForeignKey(
    AutomobileVO,
    related_name="automobile",
    on_delete=models.PROTECT,
    null=True
  )

  salesperson = models.ForeignKey(
    Salesperson,
    related_name="salesperson",
    on_delete=models.PROTECT,
    null=True
  )

  customer = models.ForeignKey(
    Customer,
    related_name="customer",
    on_delete=models.PROTECT,
    null=True
  )

  def __str__(self):
    return f"{self.sales_id}"
  
  class Meta:
    ordering = ("sales_id",)

  def get_api_url(self):
    return reverse("api_show_sale", kwargs={"pk": self.pk}) 
    

from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import (Salesperson, Customer, Sale, AutomobileVO)


class AutomobileVOEncoder(ModelEncoder):
	model = AutomobileVO
	properties = [
		"vin",
		"import_href"
	]

class SalespersonEncoder(ModelEncoder):
	model = Salesperson
	properties = [
		"id",
		"first_name",
		"last_name",
		"employee_id"
	]

class CustomerEncoder(ModelEncoder):
	model = Customer
	properties = [
		"id",
		"first_name",
		"last_name",
		"address",
		"phone_number"
	]

class SaleEncoder(ModelEncoder):
	model = Sale
	properties = [
		"price",
		"automobile",
		"salesperson",
		"customer"
	]
	encoders = {
		"automobile": AutomobileVOEncoder(),
		"salesperson": SalespersonEncoder(),
		"customer": CustomerEncoder(),
	}

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
	if request.method == "GET":
		salespeople = Salesperson.objects.all()
		return JsonResponse(
			{"salespeople": salespeople},
			encoder=SalespersonEncoder,
		)
	else:
		try:
			content = json.loads(request.body)
			salesperson = Salesperson.objects.create(**content)
			return JsonResponse(
				salesperson,
				encoder=SalespersonEncoder,
				safe=False
			)
		except Salesperson.objects.create(**content).BadRequest:
			return JsonResponse(
				{"message": "Bad request: Could not add salesperson"},
				status=400,
			)

@require_http_methods(["DELETE","GET", "PUT"])
def api_show_salespeople(request, pk):
	if request.method == "GET":
		try:
			salesperson = get_object_or_404(Salesperson.objects, id=pk)
			return JsonResponse(
				salesperson,
				encoder=SalespersonEncoder,
				safe=False
			)
		except Salesperson.DoesNotExist:
			return JsonResponse(
				{"message": "Salesperson does not exist"},
				status = 404
			)
	elif request.method == "DELETE":
		try:
			count, breakdown = Salesperson.objects.filter(id=pk).delete()
			return JsonResponse(
				{"deleted": count > 0, "breakdown": breakdown}
				)
		except Salesperson.DoesNotExist:
			return JsonResponse(
				{"message": "Salesperson does not exist"},
				status=404,
			)
	else:
		try:
			content = json.loads(request.body)
			salesperson = get_object_or_404(Salesperson.objects, id=pk)
			Salesperson.objects.filter(id=pk).update(**content)
			salesperson = Salesperson.objects.get(id=pk)
			return JsonResponse(
				salesperson, 
				encoder=SalespersonEncoder, 
				safe=False
			)
		except Salesperson.DoesNotExist:
			return JsonResponse(
				{"message": "Salesperson does not exist"},
				status=404,
			)


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
	if request.method == "GET":
		customers = Customer.objects.all()
		return JsonResponse(
			{"customers": customers},
			encoder=CustomerEncoder,
		)
	else:
		try:
			content = json.loads(request.body)
			customer = Customer.objects.create(**content)
			return JsonResponse(
				customer,
				encoder=CustomerEncoder,
				safe=False
			)
		except:
			return JsonResponse(
				{"message": "Bad request: Could not create the customer"},
				status=400,
			)	


@require_http_methods(["DELETE","GET", "PUT"])
def api_show_customer(request, pk):
	if request.method == "GET":
		try:
			customer = get_object_or_404(Customer.objects, id=pk)
			return JsonResponse(
				customer,
				encoder=CustomerEncoder,
				safe=False
			)
		except Customer.DoesNotExist:
			return JsonResponse(
				{"message": "Customer does not exist"},
				status=404,
			)
	elif request.method == "DELETE":
		try:
			count, breakdown = Customer.objects.filter(id=pk).delete()
			return JsonResponse(
				{"deleted": count > 0, "breakdown": breakdown}
				)
		except Customer.DoesNotExist:
			return JsonResponse(
				{"message": "Customer does not exist"},
				status=404,
			)
	else:
		try:
			content = json.loads(request.body)
			customer = get_object_or_404(Customer.objects, id=pk)
			Customer.objects.filter(id=pk).update(**content)
			customer = Customer.objects.get(id=pk)
			return JsonResponse(
				customer, 
				encoder=CustomerEncoder, 
				safe=False
			)
		except Customer.DoesNotExist:
			return JsonResponse(
				{"message": "Customer does not exist"},
				status=404,
			)
	
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
	if request.method == "GET":
		sales = Sale.objects.all()
		return JsonResponse(
			{"sales": sales},
			encoder=SaleEncoder,
		)
	else:

		try:
			content = json.loads(request.body)
			automobile_href = content["automobile"]
			automobile = AutomobileVO.objects.get(import_href=automobile_href)
			content["automobile"] = automobile

			salesperson_id = content["salesperson"]
			salesperson = Salesperson.objects.get(id=salesperson_id)
			content["salesperson"] = salesperson

			customer_id = content["customer"]
			customer = Customer.objects.get(id=customer_id)
			content["customer"] = customer			
		except AutomobileVO.DoesNotExist:
			return JsonResponse(
				{"message": "Automobile does not exist"},
				status=404,
			)
		except Salesperson.DoesNotExist:
			return JsonResponse(
				{"message": "Salesperson does not exist"},
				status=404,
			)
		except Customer.DoesNotExist:
			return JsonResponse(
				{"message": "Customer does not exist"},
				status=404,
			)

		try:
			sale = Sale.objects.create(**content)
			return JsonResponse(
				sale,
				encoder=SaleEncoder,
				safe=False,
			)
		except:
			return JsonResponse(
				{"message": "Bad request: Could not create the sale"},
				status=400,
			)
		

@require_http_methods(["DELETE","GET","PUT"])
def api_show_sale(request, pk):
	if request.method == "GET":
		try:
			sale = get_object_or_404(Sale.objects, id=pk)
			return JsonResponse(
				sale,
				encoder= SaleEncoder,
				safe=False
			)
		except Sale.DoesNotExist:
			return JsonResponse(
				{"message": "Sale does not exist"},
				status=404,
			)			
	elif request.method == "DELETE":
		try:
			count, breakdown = Sale.objects.filter(id=pk).delete()
			return JsonResponse(
				{"deleted": count > 0, "breakdown": breakdown}
				)
		except Sale.DoesNotExist:
			return JsonResponse(
				{"message": "Sale does not exist"},
				status=404,
			)
	else:
		try:
			content = json.loads(request.body)
			sale = get_object_or_404(Sale.objects, id=pk)
			Sale.objects.filter(id=pk).update(**content)
			sale = Sale.objects.get(id=pk)
			return JsonResponse(
				sale, 
				encoder= SaleEncoder, 
				safe=False
			)
		except Sale.DoesNotExist:
			return JsonResponse(
				{"message": "Sale does not exist"},
				status=404,
			)
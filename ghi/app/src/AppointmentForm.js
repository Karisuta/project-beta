import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';


function AppointmentForm () {
  const [appointment, setAppointment] = useState({
    date_time: '',
    reason: '',
    status: '',
    vin: '',
    technician: '',
    customer: '',
  });

  const handleAppointmentChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setAppointment({
      ...appointment,
      [inputName]: value
  });
  }

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(appointment),
      headers: {
        'Content-type': 'application/json'
      }
    }

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      setAppointment({
        date_time: '',
        reason: '',
        status: '',
        vin: '',
        technician: '',
        customer: '',
      });
      navigate('/appointments')
    }
  }

  const [technicians, setTechnicians] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div className="my-5 container">
        <Link to={'..'}>
          <button className="btn bg-2" id="goBackButton">Back to appointment list</button>
        </Link>
      </div>
      <div className="shadow p-4 mt-4">
        <h1>Create a service appointment</h1>
        <form onSubmit={handleSubmit} >
          <div className="form-floating mb-3">
            <input
            onChange={handleAppointmentChange}
            placeholder='vin'
            type='text'
            name='vin'
            className="form-control"
            />
            <label htmlFor='vin'>VIN</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleAppointmentChange}
              placeholder='customer'
              type='text'
              name='customer'
              className="form-control"
              />
              <label htmlFor='customer'>Customer</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleAppointmentChange}
            placeholder='datetime'
            type='datetime-local'
            name='date_time'
            className="form-control"
            />
            <label htmlFor='date_time'>Date and Time</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleAppointmentChange}
            placeholder='reason'
            type='text'
            name='reason'
            className="form-control"
            />
            <label htmlFor='reason'>Reason</label>
          </div>
          <div className="form-floating mb-3">
            <select onChange={handleAppointmentChange} required name='technician' id='technician' className="form-select" >
              <option value=''>Technician</option>
              {technicians.map(technician => {
                return (
                  <option key={technician.id} value={technician.id}>
                    {technician.first_name}
                  </option>
                )
              })}
            </select>
          </div>
          <button className="btn bg-2" id="newThingButton">Add</button>
        </form>
      </div>
    </div>
  )
}

export default AppointmentForm;

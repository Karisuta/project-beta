import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function AppointmentList () {

  const [appointments, setAppointments] = useState([]);
  const [appointmentsChanged, setAppointmentsChanged] = useState(false);
  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
      setAppointmentsChanged(false);
    }
  }

  useEffect(() => {
    fetchData();
  }, [appointmentsChanged]);

  const finishAppointment = async (id) => {
    fetch(`http://localhost:8080/api/appointments/${id}/finish`, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    setAppointmentsChanged(true);

  }

  const cancelAppointment = async (id) => {
    fetch(`http://localhost:8080/api/appointments/${id}/cancel`, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    setAppointmentsChanged(true);

  }

  return (
    <div>
      <div className="my-5 container">
        <Link to={{pathname: 'new'}}>
          <button className="btn bg-2" id="newThingButton">Make an appointment</button>
        </Link>
      </div>
      <div>
        <h1>Service Appointments</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {appointments.map((appointment) => {
              let datetime = appointment.date_time;
              datetime = new Date(datetime);
              if (appointment.status !== 'canceled' && appointment.status !== 'finished') {
                return (
                  <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{String(appointment.vip)}</td>
                    <td>{appointment.customer.first_name} {appointment.customer.last_name}</td>
                    <td>{datetime.toLocaleDateString()}</td>
                    <td>{datetime.toLocaleTimeString()}</td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td>{appointment.reason}</td>
                    <td>
                      <button onClick={() => cancelAppointment(appointment.id)} type='button' className='btn btn-danger'>Cancel</button>
                    </td>
                    <td>
                      <button onClick={() => finishAppointment(appointment.id)} type='button' className='btn btn-success'>Finish</button>
                    </td>
                  </tr>
                )
              } else {
                return (
                  null
                )
              }

            })}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default AppointmentList;

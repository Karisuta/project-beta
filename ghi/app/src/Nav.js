import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg bg-1">

      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/" >Home</NavLink>
            </li>
            <ul className="nav-item-dropdown dropdown-center" id="inventory-dropdown">
              <button
              className="btn bg-2 nav-link-dropdown-toggle"
              role="button"
              id="dropdownMenuButton"
              data-bs-toggle="dropdown"
              >
                Inventory
              </button>
              <div className="dropdown-menu bg-2" aria-labelledby="dropdownMenuButton">
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="manufacturers" >Manufacturers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="manufacturers/new" >New Manufacturer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="models" >Vehicle Models</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="models/new" >New Vehicle Model</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="automobiles" >Automobiles</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="automobiles/new" >New Automobile</NavLink>
                </li>
              </div>
            </ul>
            <ul className="nav-item-dropdown dropdown-center" id="sales-dropdown">
              <button
              className="btn bg-2 nav-link-dropdown-toggle"
              role="button"
              id="dropdownMenuButton"
              data-bs-toggle="dropdown"
              >
                Sales
              </button>
              <div className="dropdown-menu bg-2" aria-labelledby="dropdownMenuButton">
                <li>
                  <NavLink className="nav-link dropdown-item" to="salespeople" >Salespeople</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item" to="salespeople/new" >New Salesperson</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item" to="customers" >Customers</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item" to="customers/new" >New Customer</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item" to="sales" >Sales</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item" to="sales/new" >New Sale</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link dropdown-item" to="sales/history" >Sales History</NavLink>
                </li>
              </div>
            </ul>
            <ul className="nav-item-dropdown dropdown-center" id="service-dropdown">
              <button
              className="btn bg-2 nav-link-dropdown-toggle"
              role="button"
              id="dropdownMenuButton"
              data-bs-toggle="dropdown"
              >
                Service
              </button>
              <div className="dropdown-menu bg-2" aria-labelledby="dropdownMenuButton">
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="technicians" >Technicians</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="technicians/new" >New Technician</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="appointments" >Appointments</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="appointments/new" >New Appointment</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link dropdown-item" to="appointments/history" >Service History</NavLink>
                </li>
              </div>
            </ul>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

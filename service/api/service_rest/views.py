from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment, CustomerVO
# Create your views here.

class TechnicianListEncoder(ModelEncoder):
  model = Technician
  properties = [
    "id",
    "first_name",
    "last_name",
    "employee_id",
  ]

class CustomerVOEncoder(ModelEncoder):
  model = CustomerVO
  properties = [
    "id",
    "first_name",
    "last_name",
  ]



class AppointmentListEncoder(ModelEncoder):
  model = Appointment
  properties = [
    "id",
    "date_time",
    "reason",
    "status",
    "vip",
    "vin",
  ]
  encoder = {
    "technician": TechnicianListEncoder(),
    "customer": CustomerVOEncoder()
  }

  def get_extra_data(self, o):
    return {
      "technician": {"first_name": o.technician.first_name,
                     "last_name": o.technician.last_name,},
      "customer": {"first_name": o.customer.first_name,
                   "last_name": o.customer.last_name},
      }


class AutomobileVOEncoder(ModelEncoder):
  model = AutomobileVO
  properties = [
    "vin",
  ]


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
  if request.method == "GET":
    technicians = Technician.objects.all()
    return JsonResponse(
      {"technicians": technicians},
      encoder=TechnicianListEncoder,
    )
  else:
    content = json.loads(request.body)
    technician = Technician.objects.create(**content)
    return JsonResponse(
      technician,
      encoder=TechnicianListEncoder,
      safe=False,
    )

@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
  if request.method == "DELETE":
    count, _ = Technician.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
  if request.method == "GET":
    appointments = Appointment.objects.all()
    return JsonResponse(
      {"appointments": appointments},
      encoder=AppointmentListEncoder,
    )
  else:
    content = json.loads(request.body)

    try:
      technician_id = content["technician"]
      technician = Technician.objects.get(id=technician_id)
      content["technician"] = technician
    except Technician.DoesNotExist:
      return JsonResponse(
        {"message": "Invalid technician id"},
        status=400,
      )

    try:
      customer_name = content["customer"].split()
      customer = CustomerVO.objects.filter(first_name=customer_name[0]).get(last_name=customer_name[-1])
      content["customer"] = customer
    except CustomerVO.DoesNotExist:
      return JsonResponse(
        {"message": "Invalid customer name, please enter an existing customer name, or create a new customer first and try again."},
        status=400,
      )

    if AutomobileVO.objects.filter(vin=content["vin"]).exists():
      content["vip"] = True

    appointment = Appointment.objects.create(**content)
    return JsonResponse(
      appointment,
      encoder=AppointmentListEncoder,
      safe=False,
    )


@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
  if request.method == "DELETE":
    count, _ = Appointment.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def api_appointment_canceled(request, id):
  if request.method == "PUT":
    try:
      appointment = Appointment.objects.get(id=id)
      appointment.canceled()
      return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
      )
    except Appointment.DoesNotExist:
      return JsonResponse(
        {"message": "Appointment does not exist"},
        status=404,
      )


@require_http_methods(["PUT"])
def api_appointment_finished(request, id):
  try:
    if request.method == "PUT":
      appointment = Appointment.objects.get(id=id)
      appointment.finished()
      return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
      )
  except Appointment.DoesNotExist:
      return JsonResponse(
        {"message": "Appointment does not exist"},
        status=404,
      )


@require_http_methods(["GET"])
def api_appointments_by_vin(request, vin):
  if request.method == "GET":
    appointments = Appointment.objects.filter(vin=vin)
    return JsonResponse(
      {"appointments": appointments},
      encoder=AppointmentListEncoder,
    )
